<?php
class Osdave_PrePopulate_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getPrePopulateMessagesUrl()
    {
        return $this->_getUrl('prepopulate/adminhtml_prepopulate/getMessages');
    }
}